<?php

/*
 * Copyright (c) Adrian Jeledintan
 */

namespace Drjele\DoctrineEncrypt\Exception;

class FieldNotEncryptedException extends Exception
{
}
