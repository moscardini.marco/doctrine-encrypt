<?php

declare(strict_types=1);

/*
 * Copyright (c) Marco MOSCARDINI
 */

namespace Drjele\DoctrineEncrypt\Type;

class AES128Type extends AbstractType
{
    protected static function getShortName(): string
    {
        return 'AES128';
    }
}
