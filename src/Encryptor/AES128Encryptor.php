<?php

declare(strict_types=1);

/*
 * Copyright (c) Marco MOSCARDINI
 */

namespace Drjele\DoctrineEncrypt\Encryptor;

use Drjele\DoctrineEncrypt\Contract\EncryptorInterface;
use Drjele\DoctrineEncrypt\Exception\Exception;
use Drjele\DoctrineEncrypt\Type\AES128Type;
use Drjele\DoctrineEncrypt\Type\AES256Type;
use spec\PhpSpec\Listener\MethodReturnedNullListenerSpec;
use function Sodium\crypto_aead_aes256gcm_encrypt;

class AES128Encryptor extends AbstractEncryptor implements EncryptorInterface
{
    private const ALGORITHM = 'AES-128-ECB'; // deafult Mysql 5.7
    private const MINIMUM_KEY_LENGTH = 32;
    private const GLUE = "\0";

    public function __construct(string $salt)
    {
        if (!\is_string($salt) || \mb_strlen($salt) < static::MINIMUM_KEY_LENGTH) {
            throw new Exception('Invalid encryption salt');
        }

        parent::__construct($salt);
    }

    public function getTypeClass(): ?string
    {
        return AES128Type::class;
    }

    /**
     * Align secret to Mysql way to manage secret
     *
     * @param string $key
     * @return string
     */
    function mysql_aes_key(string $key): string
    {
        $new_key = str_repeat(static::GLUE, 16);
        for ($i = 0, $len = strlen($key); $i < $len; $i++) {
            $new_key[$i % 16] = $new_key[$i % 16] ^ $key[$i];
        }
        return $new_key;
    }

    /**
     * Encrypt using AES-128-ECB the deault on MYSQL 5.7+.
     * To orderBy or match like:
     *
     *      SET block_encryption_mode = 'aes-128-ecb';
     *      SELECT
     *          first_name as encoded,
     *          cast(
     *              aes_decrypt(
     *                  from_base64(
     *                      SUBSTR(
     *                          first_name,
     *                          6
     *                      )
     *                  ),
     *                  '1234567890123456789012345678901234'
     *              ) as char
     *          ) as decoded,
     *      FROM `persons`
     *
     * @param string $data
     * @return string
     * @throws \BadFunctionCallException
     */
    function encrypt(string $data):string
    {
        if (0 === \mb_strpos($data, static::ENCRYPTION_MARKER, 0)) {
            return $data;
        }

        if (function_exists('openssl_encrypt')) {
            $encoded = openssl_encrypt(
                $data,
                strtolower(static::ALGORITHM),
                $this->mysql_aes_key($this->salt),
                \OPENSSL_RAW_DATA
            );

            $encoded = static::ENCRYPTION_MARKER.base64_encode($encoded);
            return $encoded;
        }

        throw new \BadFunctionCallException('No encryption function could be found.');
    }

    /**
     * @param string $data
     * @return string
     * @throws \BadFunctionCallException
     */
    function decrypt(string $data): string
    {
        if (0 !== \mb_strpos($data, static::ENCRYPTION_MARKER, 0)) {
            return $data;
        }

        if (function_exists('openssl_decrypt')) {
            return openssl_decrypt(
                base64_decode(mb_substr($data,5)),
                strtolower(static::ALGORITHM),
                $this->mysql_aes_key($this->salt),
                \OPENSSL_RAW_DATA
            );
        }

        throw new \BadFunctionCallException('No decryption function could be found.');
    }

    private function generateNonce(): string
    {
        $size = \openssl_cipher_iv_length(static::ALGORITHM);

        return \random_bytes($size);
    }
}
